<h1>AutoScore Price Simulator</h1>
<h2>Pick the following numbers for each of the parameters for the simulator.
<hr>
<h3>Size</h3>
<h5>8 inch: 1</h5>
<h5>10 inch: 2</h5>
<h5>12 inch: 3</h5>
<h5>13 inch: 4</h5>
<hr>
<h3>Color</h3>
<h5>Silver: 1</h5>
<h5>Space gray: 2</h5>
<h5>Gold: 3</h5>
<hr>
<h3>Memory</h3>
<h5>256GB: 1</h5>
<h5>512GB: 2</h5>
<h5>1TB: 3</h5>
<hr>
<h3>Stand option</h3>
<h4>If you choose this option, then a stand for your tablet will come with it</h4>
<h5>With stand: 1</h5>
<h5>Without stand: 2</h5>
<hr>
<h3>Karaoke option</h3>
<h4>If you choose this option, then the sheet music will change color for where you are singing, if you have a song with lyrics</h4>
<h5>With Karaoke option: 1</h5>
<h5>Without Karaoke option: 2</h5>