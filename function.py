def pricemaker(size, color, memory, stand, karaoke):
  default = 600
  price = default
  if size==1:
    price = price * 0.9
    sizeoption = "8in"
  elif size==2:
    price = price * 1.1
    sizeoption = "10in"
  elif size==3:
    price = price*1.2
    sizeoption = "12in"
  elif size==4: 
    price = price * 1.3
    sizeoption = "13in"
  else:
    print("SIZE OPTION IS NOT CORRECT.")
  priceaftersize = price
  if color==1:
    priceaftersize = priceaftersize * 1
    coloroption = "silver"
  elif color==2:
    priceaftersize = priceaftersize * 1
    coloroption = "space gray"
  elif color==3:
    priceaftersize = priceaftersize * 1.25
    coloroption = "gold"
  else:
    print("COLOR OPTION IS NOT CORRECT.")
  priceaftercolor = priceaftersize
  if memory==1:
    priceaftercolor = priceaftercolor * 1
    memoryoption = "256GB"
  elif memory==2:
    priceaftercolor = priceaftercolor * 1.2
    memoryoption = "512GB"
  elif memory==3:
    priceaftercolor = priceaftercolor * 1.3
    memoryoption = "1TB"
  else:
    print("THE MEMORY OPTION IS NOT CORRECT.")
  priceaftermemory = priceaftercolor
  if stand == 1:
    priceaftermemory = priceaftermemory +50
    standoption = "With stand"
  elif stand==2:
    priceaftermemory = priceaftermemory * 1
    standoption = "Without stand"
  else:
    print("THE STAND OPTION IS NOT CORRECT.")
  priceafterstand = priceaftermemory
  if karaoke == 1:
    priceafterstand = priceafterstand + 80
    karaokeoption = "With karaoke option"
  elif karaoke==2:
    priceafterstand = priceafterstand * 1
    karaokeoption = "Without karaoke option"
  else:
    print("THE KARAOKE OPTION IS NOT CORRECT.")
  finalprice = priceafterstand
  print("Your tablet's price will be (in CAD):", round(finalprice, 2))
  print("*Without tax, including everything")
  print("The options you picked are listed below.")
  print("Size:", sizeoption)
  print("Color:", coloroption)
  print("Memory Size:", memoryoption)
  print("Stand option:", standoption)
  print("Karaoke option:", karaokeoption)
